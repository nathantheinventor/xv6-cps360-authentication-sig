#include "types.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "defs.h"
#include "x86.h"
#include "elf.h"
#include "fcntl.h"

#include "date.h"
#include "fs.h"
#include "spinlock.h"
#include "sleeplock.h"
#include "file.h"

int
exec(char *path, char **argv, char **env, char* cwd)
{
  char *s, *last;
  int i, off;
  uint argc, envc, sz, sp, ustack[4+MAXARG+MAXARG+1];

  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pde_t *pgdir, *oldpgdir;

  begin_op();
  if((access_things(path, 1) < 0)){
    end_op();
    return -1;
  }

  if((ip = namei(path)) == 0){ //checks absolute path
    cprintf(ALL_DEVS, "%s\n", path);
    end_op();
    return -1;
  }


  ilock(ip);
  pgdir = 0;

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) < sizeof(elf))
    goto bad;
  if(elf.magic != ELF_MAGIC)
    goto bad;

  if((pgdir = setupkvm_copy(proc->pgdir)) == 0)
    goto bad;

  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
      goto bad;
    if(ph.type != ELF_PROG_LOAD)
      continue;
    if(ph.memsz < ph.filesz)
      goto bad;
    if(ph.vaddr + ph.memsz < ph.vaddr)
      goto bad;
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
      goto bad;
    if(ph.vaddr % PGSIZE != 0)
      goto bad;
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
      goto bad;
  }
  iunlockput(ip);
  end_op();
  ip = 0;

  // Allocate two pages at the next page boundary.
  // Make the first inaccessible.  Use the second as the user stack.

/*
  if((sp = allocuvm(pgdir, KERNBASE - PGSIZE*2, KERNBASE - 1)) == 0)
    goto bad;
  clearpteu(pgdir, (char*)(KERNBASE - 2*PGSIZE));

  sp = KERNBASE - 1;
*/


/*
  //allocate 1MB stack and do things to it
  for(uint i = KERNBASE / 64 - 0x100000; i < KERNBASE / 64 - 3 * PGSIZE; i+= PGSIZE){
    mappages(pgdir, (void*)i, PGSIZE, 0, PTE_GRD);
  }
  
  if((sp = allocuvm(pgdir, KERNBASE / 64 - PGSIZE*3, KERNBASE)) == 0)
    goto bad;
  clearpteu(pgdir, (char*)(KERNBASE / 64 - 3*PGSIZE));
  
  //sp = KERNBASE - 4;
  */
  sz = PGROUNDUP(sz);
  if((sz = allocuvm(pgdir, sz, sz + 256 * PGSIZE)) == 0)
    goto bad;
  clearpteu(pgdir, (char*)(sz - PGSIZE*PGSIZE));
  sp = sz;
  //sz += 2 * PGSIZE;
  for (int i = sz - 256 * PGSIZE; i < sz - 2 * PGSIZE; i += PGSIZE) {
      uint* pte = walkpgdir(pgdir, (void *) i, 0);
      kfree((char*) KERNBASE + PTE_ADDR(*pte));
      *pte = PTE_GRD;
  }
  
  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
    if(argc >= MAXARG)
      goto bad;
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
    ustack[4+argc] = sp;
  }

  //push env onto stack
  for(envc = 0; env[envc]; envc++) {
    if(envc >= MAXARG)
      goto bad;
    sp = (sp - (strlen(env[envc]) + 1)) & ~3;
    if(copyout(pgdir, sp, env[envc], strlen(env[envc]) + 1) < 0)
      goto bad;
    ustack[4+envc+argc] = sp;
  }

  ustack[4+envc+argc] = 0;

  ustack[0] = 0xffffffff;  // fake return PC
  ustack[1] = argc;
  ustack[2] = sp - ((envc)*4) - ((argc+1)*4);  // argv pointer
  ustack[3] = sp - (envc+1)*4;  // env pointer

  sp -= (4+argc+envc+1) * 4;
  if(copyout(pgdir, sp, ustack, (4+argc+envc+1)*4) < 0)
    goto bad;
  //cprintf(ALL_DEVS, "sp: %x\n", sp);
  // Save program name for debugging.
  for(last=s=(path); *s; s++) //p decides between absolute/relative path
    if(*s == '/')
      last = s+1;
  safestrcpy(proc->name, last, sizeof(proc->name));
  safestrcpy(proc->cwdname, cwd, sizeof(proc->cwdname));

  // Commit to the user image.
  oldpgdir = proc->pgdir;
  proc->pgdir = pgdir;
  proc->sz = sz;
  proc->tf->eip = elf.entry;  // main
  proc->tf->esp = sp;
  switchuvm(proc);
  freevm(oldpgdir);
  return 0;
  
 bad:
  if(pgdir)
    freevm(pgdir);
  if(ip){
    iunlockput(ip);
    end_op();
  }
  return -1;
}

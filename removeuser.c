#include "user_tools.h"
#include "user.h"

int remove_user(int uid){
	char * filename = "users";
	int fd = open(filename,O_RDWR);

	char rd_buff[80];
	char blot[80];
	blot[0] = -1; //uid spot now 255 (-1)
	memset(blot+1,'\0',79);

	int i = 0;
	while(i < uid){
		read(fd,rd_buff,80);
		i++;
	}
	int written = write(fd,blot,80); //uid = -1 and rest 0's
	close(fd);
	if(written != 80){
		printf(1,"User Not Removed: File I/O problem\n");
		return 1;
	}
	return 0;
}

int main(int argc, char const *argv[]) {
	//check for root
	if (0 == getuid()) {
    //check if we were given a username
    if (argc < 2) {
      //no username supplied as argument
      printf(1, "Need more args.\n usage: removeuser username\n");
      return 0;
    }
    if (strlen((char*)argv[1]) >= 11) {
      //username too long, invalid
      printf(1, "Please supply a valid username\n");
      return 0;
    }

		char username[11];
    //initialize the username to all spaces
    int i=11;
    while (i--)
      username[i] = ' ';
  
    //everything's good, proceed
    strcpy(username, (char*)argv[1]);

    /*these next two lines are some fix-ups
      required because of the way that chk_username() works*/
    username[strlen((char*)argv[1])] = ' ';
    username[8] = 0;

		printf(1,"***Remove User***\n");

		int uid = chk_username(username);

		//check if no user exits or trying to remove root
		if (uid == -1){
			printf(1,"No removeable user found!\n");
			return 2;
		}else if(uid == 0){
			printf(1,"You won't like what happens if you remove root!\n");
			return -1;
		}else{//blot that user out ... make uid -1 and the rest zero out
			if(remove_user(uid) != 0){
				printf(1,"ERROR: User not Removed!\n");
				return -1;
			}
		}

		printf(1,"User Removed Successfully!\n");
		print_users();
	}else{
		printf(1,"Must have root access!\n");
		return -1;
	}

	return 0;
}
